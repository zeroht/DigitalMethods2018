﻿

1. Create a local git repository on the laptop from a folder
Go to the folder on the laptop
Command line:
crock1@crock1ubuntu:/media/crock1/MobileSSD/Documents/Gitrepos/Bagit_git$ git init

2. Link the local repository to the repository on the gitlab cloud:
Create a project on gitlab and copy the URL of the project
Go to the folder of the local repository
Command line:
repositorycrock1@crock1ubuntu:/media/crock1/MobileSSD/Documents/Gitrepos/Bagit_git$ git remote add origin https://gitlab.com/Bagit_scripts/Bagit_python_scripts.git

3. Add the local files to the local repository on the laptop :
Go to the folder of the local repository 
Command line:
crock1@crock1ubuntu:/media/crock1/MobileSSD/Documents/Gitrepos/Bagit_git$ git add .

4. Commit the changes to the local repository on the laptop :
Go to the folder of the local repository 
Command line:
crock1@crock1ubuntu:/media/crock1/MobileSSD/Documents/Gitrepos/Bagit_git$ git commit -m "Initial commit"

5. Push changes that you made in the local git repository on your computer to the gitlab cloud:
Go to the folder of the local repository
Command line: 
crock1@crock1ubuntu:/media/crock1/MobileSSD/Documents/Gitrepos/Bagit_git$ git push -u origin master

6. Pull changes that you made in the gitlab cloud down to your local repository on your laptop:
Go to the folder of the local repository
Copy the URL from the gitlab repository in the cloud
command line: 
crock1@crock1ubuntu:/media/crock1/MobileSSD/Documents/Gitrepos/Bagit_git$ git pull https://gitlab.com/Bagit_scripts/Bagit_python_scripts.git



