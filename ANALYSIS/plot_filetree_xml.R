# *************************
#
# plot_filetree_xml
#
# *************************
#
# HOW TO:
#
# a) Create an .xml-file in a Linux-shell with
#
# ''tree -X [path] > [file.xml]''
#
# b) Execute the following

require(xml2)
require(data.tree)
require(networkD3)

# create a list from the xml-file

file_list <- xml2::as_list(xml2::read_xml("filetree.xml"))

# make the first node the root of the tree
# (there happens to be a second node in the .xml-file containing a report on the directory [filepath],
# which we do not need here, and therefore will omit)

file_tree <- data.tree::FromListSimple(file_list[1])

# plot network
# (as introduced in: https://cran.r-project.org/web/packages/data.tree/vignettes/data.tree.html#plotting

library(networkD3)
fileNetwork <- ToDataFrameNetwork(file_tree, "name")
simpleNetwork(fileNetwork[-3], fontSize = 12)

# c) Use export functionality to make your plot a png or website
#
# d) enjoy
#
# **************************