#!/usr/bin/python

# This python script prepares all the files from the archive to be uploaded within a wiki by preserving traces of the initial artists archive's organisation in the new titles. This also helps overcome the issue of identical names for files initially found in different folders. If you want to reconstruct the initial archive folder structure from the files within the wiki upload folder (meaning differentiating the _ replacing / from the ones that don't), please refer to the list of files provided alongide this script as listfiles.txt to revert this process. Good luck.

# os is a library that gives us the ability to make OS changes
import os

# go into the right folder to check out what's inside
for dirname, dirnames, filenames in os.walk('CHINESE_GOLD_2006'):

#loop through the files inside the folder
    for filename in filenames:
    	#find all files
        target = os.path.join(dirname, filename)
        #change location into names
        newname = target.replace('/', '_')
        #keep the file with new name at initial location
        newnameatrightplace = dirname+'/'+newname
        # make replacements
        os.rename(target,newnameatrightplace)
